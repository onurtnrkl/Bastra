#region License
/*================================================================
Product:    Bastra
Developer:  Onur Tanrıkulu
Date:       19/09/2017 15:26

Copyright (c) 2017 Onur Tanrikulu. All rights reserved.
================================================================*/
#endregion

public interface ICardController : IController
{
    void AddCard(Card card);
    void PrintLog();
}
